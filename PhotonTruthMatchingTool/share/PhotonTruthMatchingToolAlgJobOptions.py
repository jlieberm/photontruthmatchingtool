#Skeleton joboption for a simple analysis job

#---- Minimal job options -----

jps.AthenaCommonFlags.AccessMode = "ClassAccess"              #Choose from TreeAccess,BranchAccess,ClassAccess,AthenaAccess,POOLAccess
#jps.AthenaCommonFlags.TreeName = "MyTree"                    #when using TreeAccess, must specify the input tree name

jps.AthenaCommonFlags.HistOutputs = ["MYSTREAM:myfile.root"]  #register output files like this. MYSTREAM is used in the code

athAlgSeq += CfgMgr.PhotonTruthMatchingToolAlg()                               #adds an instance of your alg to the main alg sequence


#---- Options you could specify on command line -----
#jps.AthenaCommonFlags.EvtMax=-1                          #set on command-line with: --evtMax=-1
#jps.AthenaCommonFlags.SkipEvents=0                       #set on command-line with: --skipEvents=0
jps.AthenaCommonFlags.FilesInput = ["/afs/cern.ch/work/j/jlieberm/truth/data/mc16_13TeV.423302.Pythia8EvtGen_A14NNPDF23LO_perf_JF35.deriv.DAOD_EGAM9.e3848_e5984_s3126_s3136_r10724_r10726_p4384_tid24075466_00/DAOD_EGAM9.24075466._000066.pool.root.1"]        #set on command-line with: --filesInput=...


include("AthAnalysisBaseComps/SuppressLogging.py")              #Optional include to suppress as much athena output as possible. Keep at bottom of joboptions so that it doesn't suppress the logging of the things you have configured above

