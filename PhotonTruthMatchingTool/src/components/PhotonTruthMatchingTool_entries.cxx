
#include "GaudiKernel/DeclareFactoryEntries.h"

#include "../PhotonTruthMatchingToolAlg.h"

DECLARE_ALGORITHM_FACTORY( PhotonTruthMatchingToolAlg )

DECLARE_FACTORY_ENTRIES( PhotonTruthMatchingTool ) 
{
  DECLARE_ALGORITHM( PhotonTruthMatchingToolAlg );
}
